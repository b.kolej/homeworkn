package com.bartosz.kolej.service.impl;

import com.bartosz.kolej.model.Line;
import com.bartosz.kolej.model.Point;
import com.bartosz.kolej.service.FileService;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FileServiceImplTest {

    private static final String TEST_CSV_FILE = "tests/resources/testCsv.csv";
    private static final String TEST_BINARY_FILE = "tests/resources/testBinary.bin";
    private final FileService fileService = new FileServiceImpl();

    @Test
    void shouldReadCsvFile() {
        List<String[]> result = fileService.readCsvFile(TEST_CSV_FILE);

        assertNotNull(result);
        assertEquals(2, result.size());
        assertEquals(3, result.get(0).length);
        assertEquals(2, result.get(1).length);
        assertEquals("0", result.get(0)[0]);
        assertEquals("1", result.get(0)[1]);
        assertEquals("2", result.get(0)[2]);
        assertEquals("0", result.get(1)[0]);
        assertEquals("1", result.get(1)[1]);
    }

    @Test
    void saveBinaryLines() throws IOException {
        List<Line> lineList = List.of(new Line(List.of(new Point(1, 2)), true, 1),
                new Line(List.of(new Point(5, 3)), false, 2));

        fileService.saveBinaryLinesFile(lineList, TEST_BINARY_FILE);

        assertNotNull(Files.readAllBytes(Paths.get(TEST_BINARY_FILE)));
        boolean fileDeleted = new File(TEST_BINARY_FILE).delete();
        assertTrue(fileDeleted);
    }

    @Test
    void shouldReadBinaryLineFile() {
        List<Line> lineList = List.of(new Line(List.of(new Point(1, 2)), true, 1),
                new Line(List.of(new Point(5, 3)), false, 2));

        fileService.saveBinaryLinesFile(lineList, TEST_BINARY_FILE);
        List<Line> result = fileService.readBinaryLinesFile(TEST_BINARY_FILE);

        assertNotNull(result);
        assertEquals(2, result.size());
        assertEquals(1, result.get(0).getPoints().get(0).getX());
        assertEquals(2, result.get(0).getPoints().get(0).getY());
        assertEquals(1, result.get(0).getLineId());
        assertTrue(result.get(0).isSomeFlag());
        assertFalse(result.get(1).isSomeFlag());
        assertEquals(2, result.get(1).getLineId());
        assertEquals(5, result.get(1).getPoints().get(0).getX());
        assertEquals(3, result.get(1).getPoints().get(0).getY());

        boolean fileDeleted = new File(TEST_BINARY_FILE).delete();
        assertTrue(fileDeleted);
    }
}