package com.bartosz.kolej;

import com.bartosz.kolej.model.Line;
import org.junit.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class StaticMethodTest {

    @Test
    public void shouldReturnListWithTrueFlag() {
        List<String[]> points = List.of(new String[]{"2", "3", "0"}, new String[]{"2", "3", "1"});
        List<String[]> lines = List.of(new String[]{"0", "true"}, new String[]{"1", "false"});

        List<Line> filteredList = Main.getLinesToSave(points, lines);

        assertNotNull(filteredList);
        assertEquals(1, filteredList.size());
        assertTrue(filteredList.get(0).isSomeFlag());
        assertEquals(0, filteredList.get(0).getLineId());
        assertEquals(1, filteredList.get(0).getPoints().size());
        assertEquals(2, filteredList.get(0).getPoints().get(0).getX());
        assertEquals(3, filteredList.get(0).getPoints().get(0).getY());
    }
}
