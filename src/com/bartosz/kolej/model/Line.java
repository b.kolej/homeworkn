package com.bartosz.kolej.model;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class Line implements Serializable {

    private final List<Point> points;
    private final boolean someFlag;
    private final int lineId;

    public Line(List<Point> points, boolean someFlag, int lineId) {
        this.points = points;
        this.someFlag = someFlag;
        this.lineId = lineId;
    }

    public List<Point> getPoints() {
        return points;
    }

    public boolean isSomeFlag() {
        return someFlag;
    }

    public int getLineId() {
        return lineId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Line line = (Line) o;
        return someFlag == line.someFlag && lineId == line.lineId && Objects.equals(points, line.points);
    }

    @Override
    public int hashCode() {
        return Objects.hash(points, someFlag, lineId);
    }
}