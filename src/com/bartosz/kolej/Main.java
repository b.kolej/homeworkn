package com.bartosz.kolej;

import com.bartosz.kolej.model.Line;
import com.bartosz.kolej.model.Point;
import com.bartosz.kolej.service.FileService;
import com.bartosz.kolej.service.impl.FileServiceImpl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {

    private static final String CSV_LINES_FILE_ADDRESS = "resources/lines.csv";
    private static final String CSV_POINTS_FILE_ADDRESS = "resources/points.csv";
    private static final String BIN_LINES_FILE_ADDRESS = "resources/savedLines.bin";

    public static void main(String[] args) {
        FileService fileService = new FileServiceImpl();

        List<String[]> csvPointData = fileService.readCsvFile(CSV_POINTS_FILE_ADDRESS);
        List<String[]> csvLinesData = fileService.readCsvFile(CSV_LINES_FILE_ADDRESS);

        List<Line> linesToSave = getLinesToSave(csvPointData, csvLinesData);

        fileService.saveBinaryLinesFile(linesToSave, BIN_LINES_FILE_ADDRESS);
        List<Line> linesFromFile = fileService.readBinaryLinesFile(BIN_LINES_FILE_ADDRESS);
    }

    static List<Line> getLinesToSave(List<String[]> csvPointData, List<String[]> csvLinesData) {
        Map<String, Line> linesMap = new HashMap<>();

        csvLinesData.forEach(line -> linesMap.put(line[0], new Line(new LinkedList<>(), Boolean.parseBoolean(line[1]), Integer.parseInt(line[0]))));
        csvPointData.forEach(point -> linesMap.get(point[2]).getPoints().add(new Point(Float.parseFloat(point[0]), Float.parseFloat(point[1]))));

        return linesMap.values().stream().filter(Line::isSomeFlag).collect(Collectors.toList());
    }
}
