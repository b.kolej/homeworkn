package com.bartosz.kolej.service;

import com.bartosz.kolej.model.Line;

import java.util.List;

public interface FileService {

    String RECORD_SEPARATOR = ";";

    List<String[]> readCsvFile(String fileAddress);

    List<Line> readBinaryLinesFile(String fileAddress);

    void saveBinaryLinesFile(List<Line> list, String fileAddress);
}
