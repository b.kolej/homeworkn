package com.bartosz.kolej.service.impl;

import com.bartosz.kolej.model.Line;
import com.bartosz.kolej.service.FileService;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

public class FileServiceImpl implements FileService {

    @Override
    public List<String[]> readCsvFile(String fileAddress) {
        final List<String[]> fileLines = new LinkedList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(fileAddress))) {
            String line;
            while ((line = br.readLine()) != null) {
                fileLines.add(line.split(RECORD_SEPARATOR));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileLines;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Line> readBinaryLinesFile(String fileAddress) {
        try {
            byte[] array = Files.readAllBytes(Paths.get(fileAddress));

            final ByteArrayInputStream bis = new ByteArrayInputStream(array);
            final ObjectInputStream ois = new ObjectInputStream(bis);

            return (List<Line>) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void saveBinaryLinesFile(List<Line> lineList, String fileAddress) {
        final File file = new File(fileAddress);
        final ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            final ObjectOutputStream ois = new ObjectOutputStream(bos);
            ois.writeObject(lineList);
            ois.flush();

            final FileOutputStream fos = new FileOutputStream(file);
            fos.write(bos.toByteArray());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
